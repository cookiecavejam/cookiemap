﻿using UnityEngine;

namespace Assets.Scripts
{
    public class JumpObjectScript : MonoBehaviour
    {

        private bool _canJump;
        public Vector3 TargetPosition;

        private Transform _transform;
        private Vector3 _position;

        private float _speed;
        private JumpState _state;

		public BoxCollider2D CloudCollider;

        // Use this for initialization
        void Start ()
        {
			CloudCollider = GetComponent<BoxCollider2D>();
            _canJump = true;
            _state = JumpState.Idle;
            _position = transform.position;
            _transform = transform;
        }
	
        // Update is called once per frame
        void Update () {
            if (_state != JumpState.Idle)
            {
                transform.position = Vector3.MoveTowards(transform.position, _position, Time.deltaTime * _speed);
            }
            
            if (_transform.position == _position && _state == JumpState.MovingTo)
            {
                _state = JumpState.MovingFrom;
                _position.x -= TargetPosition.x;
                _position.y -= TargetPosition.y;
            }
            if (_transform.position == _position && _state == JumpState.MovingFrom)
            {
                _state = JumpState.Idle;
                _canJump = true;
				CloudCollider.enabled = true;
            }
        }

        public void SetMoving(float playerSpeed)
        {
            _canJump = false;
            _position.x += TargetPosition.x;
            _position.y += TargetPosition.y;
            _state = JumpState.MovingTo;
            _speed = playerSpeed;
			CloudCollider.enabled = false;
        }
    }

    public enum JumpState
    {
        Idle,
        MovingTo,
        MovingFrom
    }
}
