﻿using UnityEngine;

namespace Assets.Scripts
{
	public class ChiliCookie : MonoBehaviour
	{
		public float CoolDown;
		private bool _isCollectable = true;
		public SpriteRenderer Renderer;

		// Use this for initialization
		void Start ()
		{
			_isCollectable = true;
		}

		// Update is called once per frame
		void Update () {

		}

		public void SetCollectable(bool val)
		{
			_isCollectable = val;
		}

		public bool IsCollectable()
		{
			return _isCollectable;
		}

		public void Take()
		{
			_isCollectable = false;
			GetComponent<BoxCollider2D> ().enabled = false;
			GetComponent<SpriteRenderer> ().enabled = false;
			Invoke("Show", CoolDown);
		}

		public void Show()
		{
			GetComponent<BoxCollider2D> ().enabled = true;
			GetComponent<SpriteRenderer> ().enabled = true;
			_isCollectable = true;
		}
	}
}