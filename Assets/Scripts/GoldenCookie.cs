﻿using UnityEngine;

namespace Assets.Scripts
{
    public class GoldenCookie : MonoBehaviour
    {
        
        private Vector3 _position;

        private bool _end;

        // Use this for initialization
        void Start ()
        {
            _position = transform.position + new Vector3(0, 0.8f, 0);
            _end = false;
        }
	
        // Update is called once per frame
        void Update ()
        {
            if (!_end) return;

            transform.position = Vector3.MoveTowards(transform.position, _position, Time.deltaTime * 0.2f);
        }

        public bool CanWin()
        {
            return !_end;
        }

        public bool Win()
        {
            if (_end) return false;
            _end = true;
            return _end;
        }
    }
}
