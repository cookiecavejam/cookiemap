﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MovingObject {

	public int numOfWins;

	public bool stoned;
	public bool fast;

	private Animator animator;

	// Use this for initialization
	protected override void Start() {

		animator = GetComponent<Animator>();
		numOfWins = GameManager.instance.p1Wins;

		base.Start();
		
	}

	private void OnDisable ()
	{
		//When Player object is disabled, store the current local food total in the GameManager so it can be re-loaded in next level.
		GameManager.instance.p1Wins = numOfWins;
	}

	protected override void AttemptMove <T> (int xDir, int yDir)
	{

		//Call the AttemptMove method of the base class, passing in the component T (in this case Wall) and x and y direction to move.
		base.AttemptMove <T> (xDir, yDir);

		//Hit allows us to reference the result of the Linecast done in Move.
		RaycastHit2D hit;

		//If Move returns true, meaning Player was able to move into an empty space.
		if (Move (xDir, yDir, out hit)) 
		{
			//move sound here
			//SoundManager.instance.RandomizeSfx (moveSound1, moveSound2);
		}

		//Since the player has moved and hit the golden cookie
		//CheckIfGameOver ();
	}

	protected override void OnCantMove <T> (T component)
	{
		//Set player to equal the component passed in as a parameter.
		Player otherPlayer = component as Player;

		//TODO steal Key?

		//Set the attack trigger of the player's animation controller in order to play the player's attack animation.
		//animator.SetTrigger ("playerChop");
	}

	//Restart reloads the scene when called.
	private void Restart ()
	{
		//Load the last scene loaded, in this case Main, the only scene in the game. And we load it in "Single" mode so it replace the existing one
		//and not load all the scene object in the current scene.
		SceneManager.LoadScene(0);
	}

	private void OnTriggerEnter2D (Collider2D other)
	{
		//Check if the tag of the trigger collided with is Exit.
		if(other.tag == "Win")
		{
			//win sound??
			//Invoke the Restart function to start the next level with a delay of restartLevelDelay (default 1 second).
			//Invoke ("Restart", restartLevelDelay);

			//Disable the player object since level is over.
			enabled = false;
		}

		//TODO andis weed code und so

		//Check if the tag of the trigger collided with is Food.
//		else if(other.tag == "Food")
//		{
//			//Add pointsPerFood to the players current food total.
//			food += pointsPerFood;
//
//			//Update foodText to represent current total and notify player that they gained points
//			foodText.text = "+" + pointsPerFood + " Food: " + food;
//
//			//Call the RandomizeSfx function of SoundManager and pass in two eating sounds to choose between to play the eating sound effect.
//			SoundManager.instance.RandomizeSfx (eatSound1, eatSound2);
//
//			//Disable the food object the player collided with.
//			other.gameObject.SetActive (false);
//		}
//
//		//Check if the tag of the trigger collided with is Soda.
//		else if(other.tag == "Soda")
//		{
//			//Add pointsPerSoda to players food points total
//			food += pointsPerSoda;
//
//			//Update foodText to represent current total and notify player that they gained points
//			foodText.text = "+" + pointsPerSoda + " Food: " + food;
//
//			//Call the RandomizeSfx function of SoundManager and pass in two drinking sounds to choose between to play the drinking sound effect.
//			SoundManager.instance.RandomizeSfx (drinkSound1, drinkSound2);
//
//			//Disable the soda object the player collided with.
//			other.gameObject.SetActive (false);
//		}
	}
	
	private void Update ()
	{
		int horizontal = 0;  	//Used to store the horizontal move direction.
		int vertical = 0;		//Used to store the vertical move direction.

		//Get input from the input manager, round it to an integer and store in horizontal to set x axis move direction
		horizontal = (int) (Input.GetAxisRaw ("Horizontal"));

		//Get input from the input manager, round it to an integer and store in vertical to set y axis move direction
		vertical = (int) (Input.GetAxisRaw ("Vertical"));

		//Check if moving horizontally, if so set vertical to zero.
		if(horizontal != 0)
		{
			vertical = 0;
		}

		//Check if we have a non-zero value for horizontal or vertical
		if(horizontal != 0 || vertical != 0)
		{
			AttemptMove<Wall> (horizontal, vertical);
		}
	}
}
