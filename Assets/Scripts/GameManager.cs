﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public static GameManager instance = null;	

	public BoardManager boardScript;
	public int p1Wins;
	public int p2Wins;

	// Use this for initialization
	void Awake () 
	{
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
		}

		DontDestroyOnLoad (gameObject);

		boardScript = GetComponent<BoardManager>();
		InitGame ();
	}

	void GameOver()
	{
		enabled = false;
	}

	void InitGame()
	{
		//dont load level99 pls
		boardScript.SetupScene(Intro.selectedLevel);
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(KeyCode.Escape)) 
		{ 
			Debug.Log("Escape pressed");
			Destroy (GameManager.instance);
			SceneManager.LoadScene(0);
		}
	}
}
