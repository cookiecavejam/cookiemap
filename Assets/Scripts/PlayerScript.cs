﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;
using System.Linq;

namespace Assets.Scripts
{
	public class PlayerScript : MonoBehaviour
    {
        public bool IsStoned;
        public float SpeedNormal = 1f;
        public float SpeedStoned = 1f;
        private float _additionalSpeed = 0f;

        public KeyCode WalkUp;
        public KeyCode WalkDown;
        public KeyCode WalkLeft;
        public KeyCode WalkRight;
        
        private Transform _transform;
        private Vector3 _position;
        private Vector3 _oldPosition;
        private bool _canMove;

        private Animator _animator;

        private PlayerState _state;

        public BoxCollider2D WallCollider2D;
        public BoxCollider2D ItemCollider2D;

		public string Name = "Player";
		private static int id = 1;

		public AudioSource footSteps;
	    public AudioSource HolyCookie;
	    public AudioSource Soundtrack;
	    public AudioSource CloudWush;
	    public AudioSource PowerUp;

        // Use this for initialization
        void Start ()
        {
            _position = transform.position;
            _transform = transform;
            _animator = GetComponent<Animator>();
            _state = PlayerState.Idle;
            _canMove = true;

			Name = "Player " + id++;
        }
	
        // Update is called once per frame
        void FixedUpdate ()
        {
            var speed = (IsStoned ? SpeedStoned : SpeedNormal) + _additionalSpeed;
            if (PlayerState.Winning != _state)
            {
                if (_state != PlayerState.Floating)
                {
                    if (_transform.position == _position)
                    {
                        _oldPosition = _transform.position;
                        _animator.SetInteger("Direction", (int) PlayerState.Idle);
                        _state = PlayerState.Idle;

                        Vector2? pos = null;

                        if (Input.GetKey(WalkUp))
                        {
                            _position += !IsStoned ? Vector3.up : Vector3.down;
                            _animator.SetInteger("Direction",
                                IsStoned ? (int) PlayerState.WalkingDown : (int) PlayerState.WalkingUp);
                            _state = PlayerState.WalkingUp;
                            pos = IsStoned ? Vector2.down : Vector2.up;
                        }
                        else if (Input.GetKey(WalkDown))
                        {
                            _position += !IsStoned ? Vector3.down : Vector3.up;
                            _animator.SetInteger("Direction",
                                IsStoned ? (int) PlayerState.WalkingUp : (int) PlayerState.WalkingDown);
                            _state = PlayerState.WalkingDown;
                            pos = IsStoned ? Vector2.up : Vector2.down;
                        }
                        else if (Input.GetKey(WalkLeft))
                        {
                            _position += !IsStoned ? Vector3.left : Vector3.right;
                            _animator.SetInteger("Direction",
                                IsStoned ? (int) PlayerState.WalkingRight : (int) PlayerState.WalkingLeft);
                            _state = PlayerState.WalkingLeft;
                            pos = IsStoned ? Vector2.right : Vector2.left;
                        }
                        else if (Input.GetKey(WalkRight))
                        {
                            _position += !IsStoned ? Vector3.right : Vector3.left;
                            _animator.SetInteger("Direction",
                                IsStoned ? (int) PlayerState.WalkingLeft : (int) PlayerState.WalkingRight);
                            _state = PlayerState.WalkingRight;
                            pos = IsStoned ? Vector2.left : Vector2.right;

                        }

						if (pos.HasValue)
						{
							WallCollider2D.enabled = false;
							ItemCollider2D.enabled = false;
							var raycast = Physics2D.RaycastAll(transform.position, pos.Value, 1f).ToList();
							WallCollider2D.enabled = true;
							ItemCollider2D.enabled = true;
							var temp =
								raycast.Count(x => x.collider != null && x.collider.tag == "Wall");
							if (temp > 0)
							{
								_position = _oldPosition;
								transform.position = _oldPosition;
							}
							else
							{
								footSteps.Play();
							}

						}
                    }
                }
                else if (_transform.position == _position)
                {
                    _state = PlayerState.Idle;
                    _animator.SetInteger("Direction", (int) PlayerState.Floating);
                }
                else if (_transform.position != _position)
                {
                    //player moves
                    //SoundManager.Get
                    //footSteps.Play ();
                }
            }
            else if (_state == PlayerState.Winning)
            {
                _animator.SetInteger("Direction", (int) PlayerState.Winning);
            }
            else
            {
                _animator.SetInteger("Direciton", 7);
            }
            

            transform.position = Vector3.MoveTowards(transform.position, _position, Time.deltaTime*speed);
			//sfootSteps.Play();
            
        }

        public void SpeedUp(float value, int invoke = 5)
        {
            _additionalSpeed += value;
            Invoke("ResetSpeedUp", invoke);
        }

        public void ResetSpeedUp()
        {
            _additionalSpeed = 0f;
        }

        public void SetStoned()
        {
			IsStoned = !IsStoned;
            Invoke("SetNormal", 4.202f);
        }

        public void SetNormal()
        {
            IsStoned = false;
        }

        public float GetSpeed()
        {
			return (IsStoned ? SpeedStoned : SpeedNormal) + _additionalSpeed;
        }

        public bool IsFloating()
        {
            return _state == PlayerState.Floating;
        }

        public void SetFloating()
        {
            CloudWush.Play();
            ResetSpeedUp();
            SetNormal();
            _state = PlayerState.Floating;
            _animator.SetInteger("Direction", (int)PlayerState.Floating);
        }

        public void SetJumpOffset(Vector3 offset)
        {
            _position.x += offset.x;
            _position.y += offset.y;
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {
            if (_state == PlayerState.Floating) return;

            if (collider.tag == "Player" && WallCollider2D.IsTouching(collider))
            {
                var otherPlayer = collider.gameObject.GetComponent<PlayerScript>();
                if (!otherPlayer.IsFloating())
                {
                    
                }
			} else if (collider.tag == "HashCookie" && ItemCollider2D.IsTouching(collider))
			{
				SetStoned();
				var cookie = collider.gameObject.GetComponent<HashCookie>();
				if (cookie.IsCollectable())
				{
					cookie.Take();
                    PowerUp.Play();
                }
			} else if (collider.tag == "ChiliCookie" && ItemCollider2D.IsTouching(collider))
			{
				SpeedUp(4f);
				var cookie = collider.gameObject.GetComponent<ChiliCookie>();
				if (cookie.IsCollectable())
				{
					cookie.Take();
                    PowerUp.Play();
				}
			} else if (collider.tag == "Cloud" && ItemCollider2D.IsTouching(collider))
            {
                var cloud = collider.gameObject.GetComponent<JumpObjectScript>();

                SetFloating();
                cloud.SetMoving(GetSpeed());
                SetJumpOffset(cloud.TargetPosition);
            }
            else if (collider.tag == "Wall" && WallCollider2D.IsTouching(collider))
            {
                Debug.Log("wall");
                _position = _oldPosition;
                transform.position =  _oldPosition;
            }
			else if (collider.tag == "Win" && ItemCollider2D.IsTouching(collider))
			{
				Debug.Log(this.Name + " WON!!!");
                var canWin = collider.gameObject.GetComponent<GoldenCookie>().Win();
			    if (canWin)
			    {
			        _state = PlayerState.Winning;
			        Soundtrack.Stop();
			        HolyCookie.volume = 1f;
                    HolyCookie.Play();
			        Invoke("SwitchToMainMenu", 8);
			    }
			    else
			    {
			        _state = PlayerState.Losing;
			    }
			}
        }

	    public void SwitchToMainMenu()
	    {
			Destroy (GameManager.instance);
	        SceneManager.LoadScene(0);
	    }
    }

    public enum PlayerState
    {
        Idle = 0,
        WalkingLeft = 1,
        WalkingRight = 2,
        WalkingDown = 3,
        WalkingUp = 4,
        Floating = 5,
        Winning = 6,
        Losing = 7
    }
}
