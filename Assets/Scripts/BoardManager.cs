﻿using System;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;
using UnityEngine;
using System.IO;
using System.Linq;
using Assets.Scripts;

public class BoardManager : MonoBehaviour {

	public int rows;
	public int columns;

	public GameObject cookieExit;
	public GameObject[] floorTiles;
	public GameObject[] items;

	public GameObject[] wallTiles;
	public GameObject[] outerWallTiles;

	private GameObject camObject;
	public GameObject cloud;

	private Transform boardHolder;
	private List<Vector3> gridPositions = new List<Vector3>();

	public List<List<int>> level;

	void InitializeGrid()
	{
		gridPositions.Clear();

		//Loop through x axis (columns).
		for(int x = 1; x < columns-1; x++)
		{
			//Within each column, loop through y axis (rows).
			for(int y = 1; y < rows-1; y++)
			{
				gridPositions.Add (new Vector3(x, y, 0f));
			}
		}



	}

	void SetupBoard()
	{
		boardHolder = new GameObject ("Board").transform;

		//Loop along x axis, starting from -1 (to fill corner) with floor or outerwall edge tiles.
		for(int x = -1; x < columns + 1; x++)
		{
			//Loop along y axis, starting from -1 to place floor or outerwall tiles.
			for(int y = -1; y < rows + 1; y++)
			{
				//Choose a random tile from our array of floor tile prefabs and prepare to instantiate it.
				GameObject toInstantiate = floorTiles[Random.Range (0,floorTiles.Length)];

				//Check if we current position is at board edge, if so choose a random outer wall prefab from our array of outer wall tiles.
				if(x == -1 || x == columns || y == -1 || y == rows)
					toInstantiate = outerWallTiles [Random.Range (0, outerWallTiles.Length)];

				//Instantiate the GameObject instance using the prefab chosen for toInstantiate at the Vector3 corresponding to current grid position in loop, cast it to GameObject.
				GameObject instance =
					Instantiate (toInstantiate, new Vector3 (x, y, 0f), Quaternion.identity) as GameObject;

				//Set the parent of our newly instantiated object instance to boardHolder, this is just organizational to avoid cluttering hierarchy.
				instance.transform.SetParent (boardHolder);
			}
		}
	}



	Vector3 RandomPosition ()
	{
		//not used
		int randomIndex = Random.Range (0, gridPositions.Count);
		Vector3 randomPosition = gridPositions[randomIndex];
		gridPositions.RemoveAt (randomIndex);

		return randomPosition;
	}

	public void LayoutObjectAtRandom (GameObject[] tileArray, int minimum, int maximum)
	{
		//Choose a random number of objects to instantiate within the minimum and maximum limits
		int objectCount = Random.Range (minimum, maximum+1);

		//Instantiate objects until the randomly chosen limit objectCount is reached
		for(int i = 0; i < objectCount; i++)
		{
			//Choose a position for randomPosition by getting a random position from our list of available Vector3s stored in gridPosition
			Vector3 randomPosition = RandomPosition();

			//Choose a random tile from tileArray and assign it to tileChoice
			GameObject tileChoice = tileArray[Random.Range (0, tileArray.Length)];

			//Instantiate tileChoice at the position returned by RandomPosition with no change in rotation
			Instantiate(tileChoice, randomPosition, Quaternion.identity);
		}
	}

	public void SetupScene(int levelNr)
	{
		level = new List<List<int>> ();

		LoadLevelFromCSV(levelNr);

		GameObject[] players = GameObject.FindGameObjectsWithTag ("Player");
		if (players.Length >= 2) {
			players [0].transform.position = new Vector3 (0, 0, players [0].transform.position.z);
			players [1].transform.position = new Vector3 (level.Count - 1, level.Count - 1, players [0].transform.position.z);
		}
		if (players.Length == 4) {
			players [2].transform.position = new Vector3 (level.Count - 1, 0, players [0].transform.position.z);
			players [3].transform.position = new Vector3 (0, level.Count - 1, players [0].transform.position.z);
		}
			

		//Instantiate the cookie in the middle :>
		Instantiate (cookieExit, new Vector3 (columns / 2, rows / 2, 0f), Quaternion.identity);
	}

	void LoadLevelFromCSV(int levelNumber)
	{
		Debug.Log ("Loading level " + levelNumber);
		TextAsset levelCSV = Resources.Load("Level"+levelNumber.ToString()) as TextAsset;
		string[] levelRows = levelCSV.text.Split (Environment.NewLine[0]);

		for (int i = 0; i < levelRows.Length; i++) {
			// extract each row from splitting and add it to the level
			level.Add(levelRows[i].Split(',').Select(Int32.Parse).ToList());
		}
		columns = level.First().Count;
		Debug.Log (columns);
		rows = level.Count;

		//got all data, start building board
		//reset board
		InitializeGrid();
		SetupBoard();
		//USE ONLY IF NOT IN SPLITSCREEN!!
		//     OptimizeCamera ();

		//Loop along x axis, starting from -1 (to fill corner) with floor or outerwall edge tiles.
		for(int x = 0; x < columns; x++)
		{
			//Loop along y axis, starting from -1 to place floor or outerwall tiles.
			for(int y = 0; y < rows; y++)
			{
				GameObject tile;
				int tileType = level [y][x]; //for encoding, see google drive
				// Chose random wall tile 
				switch (tileType) {

				case 0:
					tile = wallTiles [Random.Range (0, wallTiles.Length)];
					break;
				case 3:
					tile = items [Random.Range (0, items.Length)];
					break;
				default:
					tile = floorTiles [Random.Range (0, floorTiles.Length)];
					break;

				}

				if (tileType >= 100 && tileType < 200) { // is cloud beginning
					tile = cloud;
					JumpObjectScript realCloud = tile.GetComponent<JumpObjectScript>();
					realCloud.TargetPosition = getCloudOffset (tileType, new Vector3 (x, columns - y - 1, 0f));
				}

				//instanciate the chosen tile
				GameObject instance =
					Instantiate (tile, new Vector3 (x, columns - y - 1, 0f), Quaternion.identity) as GameObject;

				//Set the parent of our newly instantiated object instance to boardHolder, this is just organizational to avoid cluttering hierarchy.
				instance.transform.SetParent (boardHolder);


			}
		}
	}

	//only call with first part of cloud! (100-199)
	Vector3 getCloudOffset(int id, Vector3 originalPos)
	{
		for(int x = 0; x < columns; x++)
		{
			for(int y = 0; y < rows; y++)
			{
				if (level [y] [x] == id + 100) {
					return (new Vector3 (x, columns - y - 1, 0f) - originalPos);
				}
			}
		}
		throw new ArgumentOutOfRangeException ("Level data is corrupt, clouds without pointer exist => " + id);
	}

	void OptimizeCamera()
	{
		int cameraSize = (int)Math.Ceiling ((double)((rows + 3) / 2));
		int cameraOffset = (int)Math.Ceiling ((double)(rows / 2));
		camObject = GameObject.Find("Main Camera");
		Debug.Log (camObject.GetComponent<Camera> ().orthographicSize);
		camObject.GetComponent<Camera>().orthographicSize = cameraSize;
		Debug.Log (camObject.GetComponent<Camera> ().orthographicSize);

		Vector3 newCameraPosition = new Vector3(cameraOffset, cameraOffset, camObject.transform.position.z);
		camObject.transform.position = newCameraPosition;
		Debug.Log (camObject.transform.position.y);
	}

}
