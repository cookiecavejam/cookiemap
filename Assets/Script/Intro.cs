﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Intro : MonoBehaviour {

	public Camera camera;
	public Texture2D introLogo;
	public Texture2D mainMenuLogo;

	public Text textTitle;
	public Button buttonStartGame;
	public Button buttonExitGame;
	public Dropdown dropDownChooseLevel;
	public Dropdown dropDownChoosePlayer;

	private float fadeSpeed;
	private int drawDepth;

	private float alpha;
	private int fadeDir;

	private bool mainMenu;

	private int buttonWidth;
	private int buttonHeight;

	private int textWidth;
	private int textHeight;

	public List<string> levelList;
	public List<string> playerList;

	private bool isStartGame;
	private bool isExitGame;
	private bool isLevelSelected;
	private bool isPlayerSelected;

	public static int selectedLevel = 1;
	public static int numOfPlayers = 2;

	public Canvas canvas;

	void Awake()
	{
		camera.backgroundColor = new Color(0, 0, 0);

		mainMenu = false;

		isStartGame = false;
		isExitGame = false;
		isLevelSelected = false;
		isPlayerSelected = false;

		buttonWidth = 200;
		buttonHeight = 50;

		textWidth = 400;
		textHeight = 35;

		fadeSpeed = 0.25f;
		drawDepth = -1000;
		alpha = 0.0f;
		fadeDir = 1;
	}

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {

	}

	void OnGUI()
	{
		isStartGame = false;
		isExitGame = false;
		isLevelSelected = false;
		isPlayerSelected = false;

		if (mainMenu)
		{
			if (alpha <= 0.5f)
			{
				drawTexture(mainMenuLogo, 0.26f);
			}
			else
			{
				drawTexture(mainMenuLogo, alpha);
			}
		}

		if (mainMenu && alpha == 0.0f)
		{
			buttonStartGame.onClick.AddListener(loadLevel);
			buttonStartGame.transform.position = new Vector3(
				Screen.width / 2 - buttonWidth,
				Screen.height / 2 + buttonHeight,
				0
			);

			buttonExitGame.onClick.AddListener(exit);
			buttonExitGame.transform.position = new Vector3(
				Screen.width / 2 - buttonWidth ,
				Screen.height / 2 - buttonHeight,
				0
			);

			textTitle.transform.position = new Vector3(
				Screen.width / 2,
				Screen.height - textHeight*5 - textHeight/2,
				0
			);

			dropDownChooseLevel.onValueChanged.AddListener(delegate {
				dropDownChooseLevelChangedHandler(dropDownChooseLevel);
			});
			dropDownChooseLevel.transform.position = new Vector3(
				Screen.width / 2 + buttonWidth,
				Screen.height / 2 + buttonHeight,
				0
			);
			//Level 1 (easy)
//			Level 2 (easy)
//			Level 3 (easy)
//			Level 4 (moderate)
//			Level 5 (moderate)
//			Level 6 (moderate)
//			Level 7 (hard)
//			Level 8 (hard)
//			Level 9 (hard)
//			Level 10 (impossible)
//			Level 11 (special)
			//dropDownChooseLevel.GetComponentsInChildren<GameObject>()[0].GetComponent<Script>
			levelList = new List<string>();
			levelList.Add ("Level 1 (easy)");
			levelList.Add ("Level 2 (easy)");
			levelList.Add ("Level 3 (easy?)");
			levelList.Add ("Level 4 (moderate)");
			levelList.Add ("Level 5 (moderate)");
			levelList.Add ("Level 6 (moderate)");
			levelList.Add ("Level 7 (hard)");
			levelList.Add ("Level 8 (hard)");
			levelList.Add ("Level 9 (impossible)");
			levelList.Add ("Level 10 (special)");

			/*
            levelList.Add("Lol");
            levelList.Add("Lol");
            */
			dropDownChooseLevel.options.Clear();

			foreach (string option in levelList)
			{
				dropDownChooseLevel.options.Add(new Dropdown.OptionData(option));
			}

			dropDownChoosePlayer.onValueChanged.AddListener(delegate {
				dropDownChoosePlayerChangedHandler(dropDownChoosePlayer);
			});
			dropDownChoosePlayer.transform.position = new Vector3(
				Screen.width / 2 + buttonWidth,
				Screen.height / 2 - buttonHeight,
				0
			);
			playerList = new List<string>();

            playerList.Add("2 Players");
			playerList.Add("4 Players");

			dropDownChoosePlayer.options.Clear();

			foreach (string option in playerList)
			{
				dropDownChoosePlayer.options.Add(new Dropdown.OptionData(option));
			}

		}
		else
		{
			alpha += fadeDir * fadeSpeed * Time.deltaTime;
			alpha = Mathf.Clamp01(alpha);

			if (alpha == 1.0f)
			{
				fadeDir = -1;
				mainMenu = true;
			}

			drawTexture(introLogo, alpha);
			drawTexture(mainMenuLogo, alpha);
		}

	}

	private void drawTexture(Texture2D texture, float alpha)
	{
		GUI.color = setAlpha(alpha);
		GUI.depth = drawDepth;

		GUI.DrawTexture(
			new Rect(0, 0, Screen.width, Screen.height), texture
		);
	}

	private Color setAlpha(float alpha)
	{
		Color color = GUI.color;
		color.a = alpha;

		return color;
	}

	private void dropDownChooseLevelChangedHandler(Dropdown target)
	{
		if (!isLevelSelected)
		{
			Debug.Log("selected: " + target.value);
			selectedLevel = target.value + 1;
			isLevelSelected = true;
		}
	}

	private void dropDownChoosePlayerChangedHandler(Dropdown target)
	{
		if (!isPlayerSelected)
		{
			Debug.Log("selected: " + target.value);
			if (target.value == 0) {
				numOfPlayers = 2;
			} else if (target.value == 1) {
				numOfPlayers = 4;
			}

			isPlayerSelected = true;
		}
	}

	private void loadLevel()
	{
		if (!isStartGame)
		{
			Debug.Log("Start Game");
			Destroy (textTitle);
			if (numOfPlayers == 2) {
				SceneManager.LoadScene ("CookieCaveMap");
				Destroy (this);
			} else if (numOfPlayers == 4) {
				SceneManager.LoadScene ("4PlayerMode");
				Destroy (this);
			}
			isStartGame = true;
		}
	}

	private void exit()
	{
		if (!isExitGame)
		{
			Debug.Log("Exit Game");
			Application.Quit();
			isExitGame = true;
		}
	}
}